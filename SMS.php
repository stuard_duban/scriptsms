<?php
class SMS {

  public $url;
	public $domainId;
	public $login;
	private $user;
	private $password;
	public $debug;
	public $ua;


  __construct() {
    //Datos De prueba
    $this->url = "http://apac.soprano.com.au/cgphttp/servlet/sendmsg";
    $this->user = "test-user1@sitmobile.com";
    $this->password = "lP4tUO07";
		$this->debug = true;
  }

  public function getUrl() {
		return $this->url;
	}

	public function setUrl($val) {
		$this->url = $val;
		return $this;
	}

  public function getUser() {
		return $this->login;
	}

	public function setUser($val) {
		$this->login = $val;
		return $this;
	}
	public function getPassword() {
		return $this->password;
	}

	public function setPassword($val) {
		$this->password = $val;
		return $this;
	}

  function enviar_mensaje($numero, $mensaje) {
    $ch = curl_init();
		# this works
		//$data = array('name' => 'value');

		# this gives "Notice: Array to string conversion..."
		//$data = array('name' => array('subname' => 'subvalue'));

		//curl_setopt($ch, CURLOPT_URL, 'https://co.sopranodesign.com/cgphttp/servlet/sendmsg');
    curl_setopt($ch, CURLOPT_URL, getUrl());
    curl_setopt($ch, CURLOPT_POST, 1);
		//echo $this->getLogin() . ":" . $this->getPassword();
		//echo "destination=$destination&text=$message" . "<br>";
		curl_setopt($ch, CURLOPT_USERPWD, $this->getUser() . ":" . $this->getPassword());
		curl_setopt($ch, CURLOPT_POSTFIELDS, "destination=57$numero&text=$mensaje");
		curl_setopt($ch, CURLINFO_HEADER_OUT, true);

		//curl_setopt($ch, CURLINFO_HTTP_CODE, true);

    $httpResponse = curl_exec($ch);
    //Opcional para comprobacion
    if(curl_getinfo($ch, CURLINFO_HTTP_CODE) === 200){
			$this->logMsg("Respuesta: ".$httpResponse);

			if (strstr($httpResponse,"ERROR errNum")){
				$this->logMsg("Error enviando el SMS: ".$httpResponse);

				return false;
			}
			else
				$return = $httpResponse;
		}
		else{
			$this->logMsg("Error enviando el SMS: ".curl_error($ch).'('.curl_errno($ch).')'.$httpResponse);

			$return = false;
		}

  }
}
?>
